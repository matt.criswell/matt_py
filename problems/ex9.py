#  ex9.py
#
#  Right now this program prints
#  Age is: 30
#  Age is: 30
#  Age is: 30
#  Age is: 30
#  Age is: 30
#
#  e.g:
#  python3 ex9.py 
#  Age is: 30
#  Age is: 30
#  Age is: 30
#  Age is: 30
#  Age is: 30
#  
#  Modify the values of a, b, c, d, and e so the program prints:
#  Age is: 31
#  Age is: 38
#  Age is: 28
#  Age is: 21
#  Age is: 19
#


def my_function(age):
    print("Age is:", age)

a = 30
b = 30
c = 30
d = 30
e = 30

my_function(a)
my_function(b)
my_function(c)
my_function(d)
my_function(e)
