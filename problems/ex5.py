#  ex5.py
#
#  Right now this program has no output
#
#  e.g:
#  python3 ex5.py 
#
#
#  Modify this program so it outputs the following lines:
# 
#  Ran this function
#  Ran that function
#


def this_function():
    print("Ran this function")

def that_function():
    print("Ran that function")


