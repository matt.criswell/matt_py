#  ex8.py
#
#  Right now this program prints
#  Age is: 30
#
#  e.g:
#  python3 ex8.py 
#  Age is: 30
#  
#  Modify the variable 'a' so the program prints:
#  Age is: 40
#



def my_function(age):
    print("Age is:", age)

a = 30
my_function(a)
