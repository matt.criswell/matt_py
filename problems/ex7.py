#  ex7.py
#
#  Right now this program prints
#  Age is: 30
#
#  e.g:
#  python3 ex7.py 
#  Age is: 30
#  
#  Modify the program so it prints:
#  Age is: 30
#  Age is: 38
#  Age is: 28
#  Age is: 21
#  Age is: 19
#


def my_function(age):
    print("Age is:", age)


my_function(30)
