#  ex3.py
#
#  Right now this program prints
#
#  Ran this function
#  Ran that function
#  
#  e.g:
#  python3 ex3.py 
#  Ran this function
#  Ran that function
#
#  Modify this program so it prints each line twice, i.e:
#
#  Ran this function
#  Ran this function
#  Ran that function
#  Ran that function
#


def this_function():
    print("Ran this function")

def that_function():
    print("Ran that function")

this_function()
that_function()
