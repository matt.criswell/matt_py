#  ex4.py
#
#  Right now this program prints
#
#  Ran this function
#  Ran that function
#  
#  e.g:
#  python3 ex4.py 
#  Ran this function
#  Ran that function
#
#  Modify this program so it swaps the order of the output:
#
#  Ran that function
#  Ran this function
#

def this_function():
    print("Ran this function")

def that_function():
    print("Ran that function")

this_function()
that_function()
