#  ex6.py
#
#  Right now this program prints
#  Age is: 30
#
#  e.g:
#  python3 ex6.py 
#  Age is: 30
#  
#  Modify the program so it prints:
#  Age is: 40
#



def my_function(age):
    print("Age is:", age)


my_function(30)
